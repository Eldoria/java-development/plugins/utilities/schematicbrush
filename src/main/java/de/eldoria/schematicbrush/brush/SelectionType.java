package de.eldoria.schematicbrush.brush;

public enum SelectionType {
    REGEX, DIRECTORY
}
